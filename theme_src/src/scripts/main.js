(function ($) {
    "use strict";
    
}(jQuery)); 

// Select navigation
$(document).ready(function(){
    
    var path = window.location.pathname.split("/").pop();
    if (path == '') {
        path = 'index.php';
    }
    var target = $('nav a[href="'+path+'"]')
    target.addClass('active');


 
    $(window).scroll(function () {
        if ($(this).scrollTop() > 400) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });


  });

 
  
// Home Page slider
$('.main-slider').owlCarousel({
    loop:true,
    margin:0,
    items:1,
    nav:false,
    responsive:{
        0:{
            // items:1
        },
        600:{
            // items:3
        },
        1000:{
            // items:5
        }
    }
});

// Client Slider(Home page)
$('.client-carousel').owlCarousel({
    loop:true,
    dots: true,
    margin:10,
    nav:false,
    items:1,
    responsive:{
        0:{
            items:3,
            nav:false,
            dots: false,
            nav:true
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
});