
$.validator.addMethod("customEmail", function(value, element) {
    return value.match(/^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i);
}, $.validator.format("Please enter a valid email address."));


$.validator.addMethod("customTelephone", function (value, element) {

    if(value){
        var regex = /^\d{3}-?\d{3}-?\d{4}$/;
        if(! regex.test(value)){
            // alert(value);
            return false;
        } else{
            return true;
        }
    } else{
        return true;
    }

    // if ( /^\d{3}-?\d{3}-?\d{4}$/g.test(value)) {
    //     return true;
    // } else {
    //     return false;
    // };
}), ("Invalid phone number");

$.validator.addMethod("customDate", function(value, element) {
    return value.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
}, $.validator.format("Please enter a date in the format dd/mm/yyyy."));





(function( $ ) {

    $('#contact-form').submit(function(e){
        e.preventDefault();
    });

    $('#contact-form').validate({
        ignore: [],
        rules: {
            userName: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
            },
            email: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
                customEmail: true,
                maxlength: 100,
            },
            phone: {
                required: false,
                number: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
            },
            arrivaldate: {
                required: false,
                customDate: true,
            },
            nightcount: {
                required: false,
                number: true,
            },
            adults: {
                required: false,
                number: true,
            },
            children: {
                required: false,
                number: true,
            },
            messagearea: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
                maxlength: 500,
            }
        },
        messages: {
            phone:{
                customTelephone: "Please enter a valid number",
            },
            phone:{
                customDate: "Please enter date in this format",
            }
        },
        submitHandler: function(form) {
            form.submit();
            // $.ajax({
            //     url: '',
            //     type: 'POST',
            //     dataType: 'JSON',
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     },
            //     data: $(form).serialize(),
            //     beforeSend: function () {
            //         preloaderShow();
            //     },
            //     complete: function () {
            //         preloaderHide();
            //     },
            //     success: function(response){
            //         if(response.success){
            //             $(form).find('.alert-success').removeClass('hide');
            //         } else{
            //             $(form).find('.alert-error').removeClass('hide');
            //         }
            //     }
            // });
        }
    });


}(jQuery));