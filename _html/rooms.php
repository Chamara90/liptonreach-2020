<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>Lipton Reach</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid p-0">
        <div class="inner-page-cover">
            <div class="inner-page-cover-img" style="background-image: url('assets/images/rooms-cover.jpg')">
            
            </div>
        </div>
    </div>

    <main id="elements-page" class="main-content homepage-main-content pt-0">

    <div class="pt-4">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="inner-main-title">Rooms</h1>
                </div>
            </div>
        </div>
    </div>

        <div class="page-section facility-info-section pt-1 pb-4">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="pb-3 pb-md-0">
                            <img alt="Room Description Image" src="assets/images/room-desc-img.jpg" class="img-fluid">
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="">
                            <p>Set in Bandarawela, 28 km from NuwaraEliya, Lipton Reach features a barbecue and views 
                                of the mountains. Free WiFi is offered and free private parking is available on site.
                                Living area has a large a flat-screen TV with DVD. Rooms come with a private bathroom 
                                equipped with a shower. You will find a shared kitchen at the property. Ella is 8 km from 
                                Lipton Reach, while Udawalawe is 49 km from the property.It's easy to reach Lipton Reach 
                                using public transport. If you're travelling by car, you'll find 
                                parking available at the property. Lipton Reach is a private villa. It features a garden, 
                                stunning mountain views and exquisite furnishings. The spacious villa can sleep up to 10 
                                guests and has a living area with a sofa and a flat-screen cable TV and DVD player. 
                                For recreation a Carrom Board, Childrens DVDs, Badminton nets and shuttlecocks, 
                                3 Bicycles to ride and a barbecue facility is available.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section room-type pt-3 pt-md-4 pb-2 pb-md-4">
            <div class="container">
                <div class="inner-topic">
                    <h3 class="text-uppercase mb-3">Rose</h3>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="room-type-img">
                            <img alt="Room Description Image" src="assets/images/rose-1.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="room-type-img">
                            <img alt="Room Description Image" src="assets/images/rose-2.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="room-type-img">
                            <img alt="Room Description Image" src="assets/images/rose-3.jpg" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section room-type pt-3 pt-md-4 pb-2 pb-md-4">
            <div class="container">
                <div class="inner-topic">
                    <h3 class="text-uppercase mb-3">Lilly</h3>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="room-type-img">
                            <img alt="Room Description Image" src="assets/images/lilly-1.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="room-type-img">
                            <img alt="Room Description Image" src="assets/images/lilly-2.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="room-type-img">
                            <img alt="Room Description Image" src="assets/images/lilly-3.jpg" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section room-type pt-3 pt-md-4 pb-2 pb-md-4">
            <div class="container">
                <div class="inner-topic">
                    <h3 class="text-uppercase mb-3">Daffodil</h3>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="room-type-img">
                            <img alt="Room Description Image" src="assets/images/daffodil-1.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="room-type-img">
                            <img alt="Room Description Image" src="assets/images/daffodil-2.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="room-type-img">
                            <img alt="Room Description Image" src="assets/images/daffodil-3.jpg" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section room-type pt-3 pt-md-4 pb-2 pb-md-4">
            <div class="container">
                <div class="inner-topic">
                    <h3 class="text-uppercase mb-3">Jasmine</h3>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="room-type-img">
                            <img alt="Room Description Image" src="assets/images/jasmine-1.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="room-type-img">
                            <img alt="Room Description Image" src="assets/images/jasmine-2.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="room-type-img">
                            <img alt="Room Description Image" src="assets/images/jasmine-3.jpg" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

    </main>

    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

</body>
</html>
