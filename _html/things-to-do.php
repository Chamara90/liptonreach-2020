<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>Lipton Reach</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid p-0">
        <div class="inner-page-cover">
            <div class="inner-page-cover-img" style="background-image: url('assets/images/things-to-do.jpg')">
            
            </div>
        </div>
    </div>

    <main id="elements-page" class="main-content homepage-main-content pt-0">

        
        <div class="page-section room-type pt-4 pb-2 pb-md-4">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="inner-main-title">Things to do in Bandarawela</h1>
                    </div>
                </div>
            </div>
            <div class="container">
                <h3 class="tdo-topic-1">Ella - 15 minutes</h3>
                <h3 class="tdo-topic-2 mt-3">What to see in Ella</h3>
            </div>            
        </div>

        <div class="page-section things-to-section pb-2 pb-md-4">
            <div class="container">
                <h3 class="tdo-topic-3 mb-3">Ella Rock</h3>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="things-to-img">
                            <img alt="Things to do image" src="assets/images/ella-rock.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="mt-2 mt-sm-0">
                            <p>The magnificent views of Haputale which is unique to any other destination in the country. The whole area is sometimes covering with 
                            mist and within few minutes you may see the clear views as well. In a clear day, one can see the landscapes up to the distance of 
                            southern beach from Haputale.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section things-to-section pb-2 pb-md-4">
            <div class="container">
                <h3 class="tdo-topic-3 mb-3">Little Adam's Peak</h3>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="things-to-img">
                            <img alt="Things to do image" src="assets/images/little-adamspeak.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="mt-2 mt-sm-0">
                            <p>The magnificent views of Haputale which is unique to any other destination in the country. The whole area is sometimes covering with 
                            mist and within few minutes you may see the clear views as well. In a clear day, one can see the landscapes up to the distance of 
                            southern beach from Haputale.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section things-to-section pb-2 pb-md-4">
            <div class="container">
                <h3 class="tdo-topic-3 mb-3">Nine Arched Bridge</h3>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="things-to-img">
                            <img alt="Things to do image" src="assets/images/nine-arched-bridge.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="mt-2 mt-sm-0">
                            <p>The magnificent views of Haputale which is unique to any other destination in the country. The whole area is sometimes covering with 
                            mist and within few minutes you may see the clear views as well. In a clear day, one can see the landscapes up to the distance of 
                            southern beach from Haputale.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section things-to-section pb-2 pb-md-4">
            <div class="container">
                <h3 class="tdo-topic-3 mb-3">Ravana Waterfall and Caves</h3>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="things-to-img">
                            <img alt="Things to do image" src="assets/images/ravana-falls.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="mt-2 mt-sm-0">
                            <p>The magnificent views of Haputale which is unique to any other destination in the country. The whole area is sometimes covering with 
                            mist and within few minutes you may see the clear views as well. In a clear day, one can see the landscapes up to the distance of 
                            southern beach from Haputale.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="page-section room-type pt-4 pb-2">
            <div class="container">
                <h3 class="tdo-topic-1 mt-3">Lipton Seat - 30 minutes</h3>
            </div>            
        </div>


        <div class="page-section things-to-section pb-2 pb-md-4">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="things-to-img">
                            <img alt="Things to do image" src="assets/images/lipton-seat.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="mt-2 mt-sm-0">
                            <p>The magnificent views of Haputale which is unique to any other destination in the country. The whole area is sometimes covering with 
                            mist and within few minutes you may see the clear views as well. In a clear day, one can see the landscapes up to the distance of 
                            southern beach from Haputale.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="page-section room-type pb-2">
            <div class="container">
                <h3 class="tdo-topic-1 mt-3">Horton Plane's & World's End - One hour & 15 minutes</h3>
            </div>            
        </div>


        <div class="page-section things-to-section pb-2 pb-md-4">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="things-to-img">
                            <img alt="Things to do image" src="assets/images/horton-plains.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="mt-2 mt-sm-0">
                            <p>The magnificent views of Haputale which is unique to any other destination in the country. The whole area is sometimes covering with 
                            mist and within few minutes you may see the clear views as well. In a clear day, one can see the landscapes up to the distance of 
                            southern beach from Haputale.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="page-section room-type pb-2">
            <div class="container">
                <h3 class="tdo-topic-1 mt-3">Amaizing Train Journey</h3>
            </div>            
        </div>


        <div class="page-section things-to-section pb-2 pb-md-4">
            <div class="container">
                <h3 class="tdo-topic-2">The most popular train journey in Sri Lanka - Kandy to Ella part</h3>
            </div> 
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="things-to-img">
                            <img alt="Things to do image" src="assets/images/the-train-journey.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="mt-2 mt-sm-0">
                            <p>The magnificent views of Haputale which is unique to any other destination in the country. The whole area is sometimes covering with 
                            mist and within few minutes you may see the clear views as well. In a clear day, one can see the landscapes up to the distance of 
                            southern beach from Haputale.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        

        

        

        <!-- <div class="page-section room-type pt-4 pb-2 pb-md-4">
            <div class="container">
                <h3>Lilly</h3>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="things-to-img">
                            <img alt="Room Description Image" src="assets/images/room-desc-img.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="things-to-img">
                            <img alt="Room Description Image" src="assets/images/room-desc-img.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="things-to-img">
                            <img alt="Room Description Image" src="assets/images/room-desc-img.jpg" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        

        

    </main>

    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

</body>
</html>
