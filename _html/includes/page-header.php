<header id="header">

  <div class="container-fluid top-info">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="logo-container pt-1 pt-md-3 pb-1 pb-md-3 text-center text-md-left">
            <a href="index.php"><img alt="Logo" class="main-logo" src="assets/images/lipton-reach-logo.png"></a>
          </div>
        </div>
        <div class="col-md-8">
          <div class="top-contact-wrap d-flex justify-content-end align-items-center">
            <div class="top-contact-container d-none d-md-block">
                <ul class="top-contact">
                  <li><i class="fa fa-map-marker" aria-hidden="true"></i> No 48, Kebillewela South,  Bandarawela, Sri Lanka</li>
                  <li> <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 49.999 48.221">
                        <g id="telephone" transform="translate(0 -9.1)">
                          <g id="Group_2" data-name="Group 2" transform="translate(0 9.1)">
                            <g id="Group_1" data-name="Group 1" transform="translate(0 0)">
                              <path id="Path_1" data-name="Path 1" d="M48.083,18.421c-1.919-2.64-5.278-5.023-9.46-6.709a37.015,37.015,0,0,0-27.211-.041c-4.189,1.674-7.557,4.046-9.483,6.68A8.578,8.578,0,0,0,.388,26.235,4.132,4.132,0,0,0,4.348,29.09l6.245.006H10.6a4.172,4.172,0,0,0,3.21-1.5,4.1,4.1,0,0,0,.894-3.374q-.01-.053-.023-.105l-.329-1.278A25.479,25.479,0,0,1,25,20.324a26.156,26.156,0,0,1,10.647,2.569l-.334,1.27q-.014.054-.024.109a4.1,4.1,0,0,0,.884,3.376,4.172,4.172,0,0,0,3.213,1.506l6.245.006h.006A4.132,4.132,0,0,0,49.6,26.316,8.586,8.586,0,0,0,48.083,18.421Z" transform="translate(0 -9.1)" fill="#000"/>
                            </g>
                          </g>
                          <g id="Group_4" data-name="Group 4" transform="translate(5.218 26.657)">
                            <g id="Group_3" data-name="Group 3">
                              <path id="Path_2" data-name="Path 2" d="M87.334,197.768l-4.657-4.89a1.492,1.492,0,0,0-1.08-.463h-3.01v-2.034a1.492,1.492,0,1,0-2.984,0v2.034H70.825v-2.034a1.492,1.492,0,0,0-2.984,0v2.034h-3.01a1.492,1.492,0,0,0-1.08.463l-4.657,4.89a20.44,20.44,0,0,0-5.662,14.156v6.138a1.492,1.492,0,0,0,1.492,1.492H91.5A1.492,1.492,0,0,0,93,218.062v-6.138A20.44,20.44,0,0,0,87.334,197.768ZM73.214,214.1a7.808,7.808,0,1,1,7.808-7.808A7.817,7.817,0,0,1,73.214,214.1Z" transform="translate(-53.432 -188.89)" fill="#000"/>
                            </g>
                          </g>
                          <g id="Group_6" data-name="Group 6" transform="translate(20.176 39.235)">
                            <g id="Group_5" data-name="Group 5" transform="translate(0 0)">
                              <path id="Path_3" data-name="Path 3" d="M211.427,317.691a4.824,4.824,0,1,0,4.824,4.824A4.83,4.83,0,0,0,211.427,317.691Z" transform="translate(-206.603 -317.691)" fill="#000"/>
                            </g>
                          </g>
                        </g>
                      </svg> +94 112 575 661</li>
                  <li><i class="fa fa-phone" aria-hidden="true"></i> +94 777 386686</li>
                </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <nav class="navbar navbar-expand-md d-flex justify-content-center">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="rooms.php">Rooms</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="facilities.php">Facilities</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="things-to-do.php">Things to Do</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="gallery.php">Gallery</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contact.php">Contact</a>
          </li>
        </ul>
      </div>
    </nav>

  </div>
</header>
