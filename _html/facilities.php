<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>Lipton Reach</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid p-0">
        <div class="inner-page-cover">
            <div class="inner-page-cover-img" style="background-image: url('assets/images/facility-cover.jpg')">
            
            </div>
        </div>
    </div>

    <main id="elements-page" class="main-content homepage-main-content pt-0">

        <div class="page-section room-type pt-4 pb-4">

        <div class="pt-4">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="inner-main-title">The Living Area</h1>
                    </div>
                </div>
            </div>
        </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="facility-image">
                            <img alt="Room Facility Image" src="assets/images/facility-img.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div>
                            <p>Set in Bandarawela, 28 km from NuwaraEliya, Lipton Reach features a barbecue and views 
                                of the mountains. Free WiFi is offered and free private parking is available on site.
                                Living area has a large a flat-screen TV with DVD. Rooms come with a private bathroom 
                                equipped with a shower. You will find a shared kitchen at the property. Ella is 8 km from 
                                Lipton Reach, while Udawalawe is 49 km from the property.It's easy to reach Lipton Reach 
                                using public transport. If you're travelling by car, you'll find 
                                parking available at the property. Lipton Reach is a private villa. It features a garden, 
                                stunning mountain views and exquisite furnishings. The spacious villa can sleep up to 10 
                                guests and has a living area with a sofa and a flat-screen cable TV and DVD player. 
                                For recreation a Carrom Board, Childrens DVDs, Badminton nets and shuttlecocks, 
                                3 Bicycles to ride and a barbecue facility is available.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <div class="page-section pt-4 pb-0 pb-md-4">            
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="room-facility-img">
                            <img alt="Room Description Image" src="assets/images/sofa-set.jpg" class="img-fluid">
                            <h4>Sofa Set</h4>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="room-facility-img">
                            <img alt="Room Description Image" src="assets/images/tv-home-theater.jpg" class="img-fluid">
                            <h4>LED TV and Home Theatre</h4>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="room-facility-img">
                            <img alt="Room Description Image" src="assets/images/dining-table.jpg" class="img-fluid">
                            <h4>Dining Table</h4>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="room-facility-img">
                            <img alt="Room Description Image" src="assets/images/comfort-chair.jpg" class="img-fluid">
                            <h4>Comfortable Chairs</h4>
                        </div>
                    </div>  
                    <div class="col-sm-4">
                        <div class="room-facility-img">
                            <img alt="Room Description Image" src="assets/images/wooden-wall-furnitures.jpg" class="img-fluid">
                            <h4>Wooden Walls and Wooden Furnitures</h4>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="room-facility-img">
                            <img alt="Room Description Image" src="assets/images/antiques.jpg" class="img-fluid">
                            <h4>Antiques</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section kitchen-facility pt-0 pt-md-4 pb-0 pb-md-4">
            <div class="container">
               <div class="inner-topic">
                    <h3 class="text-capitalize mb-3">Kitchen & Dining</h3>
               </div>   
               <p class="kd-description">There is a well equipped kitchen with all cooking utensils such as Microwave oven, gas cooker, rice cooker, cutlery and crockery along with refrigerator.
                A cook can be provided on prior request.<br>
                Food can be prepared by you using the kitchen and cooking utensils by food provisions provided by you.<br>
                Alternatively, you may order food from our bungalow menu.
                </p>             
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="room-facility-img">
                            <img alt="Room Description Image" src="assets/images/kitchen-1.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="room-facility-img">
                            <img alt="Room Description Image" src="assets/images/kitchen-2.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="room-facility-img">
                            <img alt="Room Description Image" src="assets/images/kitchen-3.jpg" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section room-type pt-2 pt-md-4 pb-0 pb-md-4">
            <div class="container">
               <div class="inner-topic">
                    <h3 class="text-capitalize mb-3">The Garden Setting</h3>
               </div>                
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="facility-image">
                            <img alt="Room Description Image" src="assets/images/garden-setting.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mt-3 mt-md-0">
                            <p>Set in Bandarawela, 28 km from NuwaraEliya, Lipton Reach features a barbecue and views 
                            of the mountains. Free WiFi is offered and free private parking is available on site.
                            Living area has a large a flat-screen TV with DVD. Rooms come with a private bathroom 
                            equipped with a shower. You will find a shared kitchen at the property. Ella is 8 km from 
                            Lipton Reach, while Udawalawe is 49 km from the property.It's easy to reach Lipton Reach 
                            using public transport. If you're travelling by car, you'll find 
                            parking available at the property. Lipton Reach is a private villa. It features a garden, 
                            stunning mountain views and exquisite furnishings. The spacious villa can sleep up to 10 
                            guests and has a living area with a sofa and a flat-screen cable TV and DVD player. 
                            For recreation a Carrom Board, Childrens DVDs, Badminton nets and shuttlecocks, 
                            3 Bicycles to ride and a barbecue facility is available.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container mt-3 mt-md-5">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="room-facility-img">
                            <img alt="Room Description Image" src="assets/images/garden-setting-1.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="room-facility-img">
                            <img alt="Room Description Image" src="assets/images/garden-setting-2.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-4">  
                        <div class="room-facility-img">
                            <img alt="Room Description Image" src="assets/images/garden-setting-3.jpg" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section pt-0 pt-md-4 pb-4">
            <div class="container">
                <div class="inner-topic">
                    <h3>Other Facilities</h3>
                </div>                

                <ul class="facility-list">
                    <li><p>Free WI-FI</p></li>
                    <li><p>Parking is available within the property.</p></li>
                    <li><p>transport facility (airport pick up and drop, railway station pickup and drop, day tours, round tours).</p></li>
                </ul>
            </div>            
        </div>

        <div class="page-section room-type pt-0 pt-md-4 pb-0 pb-md-4">
            <div class="container">
               <div class="inner-topic">
                    <h3 class="text-capitalize mb-3">Servant & Driver rooms</h3>
               </div>                
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="room-facility-img">
                            <img alt="Room Description Image" src="assets/images/servant-driver-rom-1.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="room-facility-img">
                            <img alt="Room Description Image" src="assets/images/servant-driver-rom-2.jpg" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="room-facility-img">
                            <p>There are two rooms seperately located within the bungalow for the servant 
                                and driver. Additional bathrooms (casual bathroom) is also there for the 
                                useage of them.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="page-section pt-0 pt-md-4 pb-4">
            <div class="container">
                <div class="inner-topic">
                    <h3>Day tours & Round tours</h3>
                </div>
                <br>
                <p>Ella & around, Nuwara Eliya tour, Haputale tour, train journey, Yala, Udawalawe, Tangalle beach tour, Mirissa Whale watching tours are favorite day tours you can do within a day when you 
                    are at Highcliffe Bungalow.</p>
                 <br>
                <p>In addition, we do round tours starting from airport pick up if required. We have a fleet of vehicles starting from budget cars to luxury vans. Special discounted rates will be given to guest who takes accommodation at Highcliffe Bungalow.</p>
            </div>            
        </div>

        <!-- <p class="imglist" style="max-width: 1000px;">
        <a href="https://source.unsplash.com/juHayWuaaoQ/1500x1000" data-fancybox="images">
            <img src="https://source.unsplash.com/juHayWuaaoQ/240x160" />
        </a>

        <a href="https://source.unsplash.com/eWFdaPRFjwE/1500x1000" data-fancybox="images">
            <img src="https://source.unsplash.com/eWFdaPRFjwE/240x160" />
        </a>
        
        </p> -->
        

        

    </main>

    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

<script>
    $(document).ready(function() {
    // $("a.fancybox").fancybox();
    });
</script>


</body>
</html>
