<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>Lipton Reach</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid p-0">
        <div class="inner-page-cover">
            <div class="inner-page-cover-img" style="background-image: url('assets/images/gallery-cover.jpg')">
            
            </div>
        </div>
    </div>

    <main id="elements-page" class="main-content homepage-main-content pt-0">

        

        <div class="page-section facility-info-section pt-4 pb-1 pb-md-4">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-right w-100">
                        <a class="btn btn-primary br-btn">Videos</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section gallery pt-1 pt-md-2 pb-4">
            
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                    <p class="imglist">
                        <a class="fancybox-item image-item" href="assets/images/gallery/2-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/2.jpg" />
                        </a>

                        <a class="fancybox-item image-item" href="assets/images/gallery/3-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/3.jpg" />
                        </a>

                        <a class="fancybox-item image-item" href="assets/images/gallery/4-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/4.jpg" />
                        </a>

                        <a class="fancybox-item image-item" href="assets/images/gallery/5-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/5.jpg" />
                        </a>

                        <a class="fancybox-item image-item" href="assets/images/gallery/6-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/6.jpg" />
                        </a>

                        <a class="fancybox-item image-item" href="assets/images/gallery/7-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/7.jpg" />
                        </a>

                        <a class="fancybox-item image-item" href="assets/images/gallery/8-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/8.jpg" />
                        </a>

                        <a class="fancybox-item image-item" href="assets/images/gallery/9-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/9.jpg" />
                        </a>

                        <a class="fancybox-item image-item" href="assets/images/gallery/10-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/10.jpg" />
                        </a>

                        <a class="fancybox-item image-item" href="assets/images/gallery/11-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/11.jpg" />
                        </a>

                        <a class="fancybox-item image-item" href="assets/images/gallery/12-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/12.jpg" />
                        </a>

                        <a class="fancybox-item image-item" href="assets/images/gallery/13-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/13.jpg" />
                        </a>

                        <a class="fancybox-item image-item" href="assets/images/gallery/14-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/14.jpg" />
                        </a>

                        <a class="fancybox-item image-item" href="assets/images/gallery/15-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/15.jpg" />
                        </a>

                        <a class="fancybox-item image-item" href="assets/images/gallery/16-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/16.jpg" />
                        </a>

                        <a class="fancybox-item image-item" href="assets/images/gallery/17-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/17.jpg" />
                        </a>

                        <a class="fancybox-item image-item" href="assets/images/gallery/18-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/18.jpg" />
                        </a>

                        <a class="fancybox-item image-item" href="assets/images/gallery/19-1.jpg" data-fancybox="images">
                            <img class="img-fluid" src="assets/images/gallery/19.jpg" />
                        </a>
                    </p>
                    </div>    
                </div>
            </div>

        </div>
        

    </main>

    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

<script>
    $(document).ready(function() {
        $(".fancybox-item").fancybox({
            // padding: 10,
        });
    });
</script>


</body>
</html>
