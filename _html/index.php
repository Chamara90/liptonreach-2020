<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>Lipton Reach</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid p-0">
        <div class="home-slider">
            <div class="home-main-slider owl-carousel">
                <div class="item">
                    <div class="slider-img" style="background-image: url('assets/images/slider-img-1.jpg')">                        
                    </div>
                </div>
                <!-- <div class="item">
                    <div class="slider-img" style="background-image: url('assets/images/slider-img-2.jpg')">                        
                    </div>
                </div> -->
            </div>
        </div>
    </div>

    <main id="elements-page" class="main-content homepage-main-content pt-0">

        <div class="page-section pt-2 pt-md-5">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="home-caption text-center w-100">An exceptional stay in Ella & Bandarawela</h1>
                        <p class="text-center w-100 d-none d-md-block">Set in Bandarawela, 28km from Nuwara Eliya, Lipton Reach features a barbecue and view of the mountains.<br>
                        Free WIFI is offered and free parivate parking is available on site. Living area has a large flat-screen 
                        TV with DVD.<br> Rooms come with a private bathroom equipped with a shower. You will find a shared kitchen at the
                        property. Ella is 8km from Lipton Reach,<br> while Udawalawe is 49km from the property.</p>

                        <p class="text-center w-100 d-block d-md-none pl-4 pl-md-0 pr-4 pr-md-0">Welcome to your Luxurious self catering holiday bungalow in  Ella – Bandarawela which is a colonial styled bungalow 
                        refurbished to offer modern luxury at its  best. Nested in its own well landscaped garden setting, this offers a unique 
                        experiencing of a luxury hideout just 1 km from the city centre of Bandarawela.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-section facility-info-section pt-4 pb-4">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="facility-info mb-4 mb-md-0">
                            <h3>Luxurious Living Area</h3>
                            <img class="img-fluid" alt="Facility image" src="assets/images/room2-demo3.jpg">
                            <p>Spacious, fully furnished with modern luxury, value<br> added and enhanced by adding antiques of colonial<br> times, with large LED TV and many more</p>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="facility-info mb-4 mb-md-0">
                            <h3>Elegant Rooms</h3>
                            <img class="img-fluid" alt="Facility image" src="assets/images/room3-demo3.jpg">
                            <p>The huge wooden four poster and canopied beds with<br> added modern comfort, Kumbuk wood flooring, superior<br> bathrooms of ultra modern design</p>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="facility-info">
                            <h3>Heavenly Garden </h3>
                            <img class="img-fluid" alt="Facility image" src="assets/images/room3-demo1.jpg">
                            <p>Beautiful garden setting with lawn surrounded by trees,<br> flower plants, sitting areas,<br> with colorful light fixing</p>
                        </div>
                    </div>
                </div>

                <div class="facility-border-shadow">
                    <div class="inner-shadow"></div>
                </div>
            </div>            
        </div>


        <div class="page-section attraction-section pt-4 pb-4">
            <div class="inner-topic text-center text-uppercase pb-3"><h3>Attractions</h3></div>
            <div class="container">            
                <div class="row">
                    <div class="col-sm-3">
                        <div class="attraction-info mb-5 mb-md-0">                            
                            <img class="img-fluid" alt="Facility image" src="assets/images/ella.jpg">
                            <h4>Ella</h4>
                            <p>The secnic mountain village, with lots of attractions such as Little Adamspeak, Ravana Falls, Nine Arched Bridge, is a perfect base for relasxing.</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="attraction-info mb-5 mb-md-0">                            
                            <img class="img-fluid" alt="Facility image" src="assets/images/haputale.jpg">
                            <h4>Haputale</h4>
                            <p>The magnificient views which are unique to any destination in the country. Shoart drives to Adhisham Bungalow, Dambetenne Tea Factory, etc...</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="attraction-info mb-5 mb-md-0">                            
                            <img class="img-fluid" alt="Facility image" src="assets/images/horton-plains.jpg">
                            <h4>Horton Plains</h4>
                            <p>The most secnic and the heighest Nationa Park in Sri Lanka. It includes World's End, Bakers Falls waterfall, Kirigalpoththa mountain, a unique eco system, etc...</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="attraction-info">                            
                            <img class="img-fluid" alt="Facility image" src="assets/images/the-train-journey.jpg">
                            <h4>Train Journey</h4>
                            <p>Kandy to Ella train trip through Bandarawela is considered as one of the most secnic train journey in the world. Stay with us after the journey.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        

    </main>

    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

<script>
    $('.home-main-slider').owlCarousel({
    items:1,
    loop:false,
    margin:10,
    nav:true,
    autoplay:true,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    // responsive:{
    //     0:{
    //         items:2
    //     },
    //     600:{
    //         items:2
    //     },
    //     1000:{
    //         items:2
    //     }
    // }
})
</script>


</body>
</html>
